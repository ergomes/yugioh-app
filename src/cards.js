export const cards = [
  {
    "id": 83994646,
    "name": "4-Starred Ladybug of Doom",
    "type": "Flip Effect Monster",
    "desc": "FLIP: Destroy all Level 4 monsters your opponent controls.",
    "atk": 800,
    "def": 1200,
    "level": 3,
    "race": "Insect",
    "attribute": "WIND",
    "card_sets": [
      {
        "set_name": "Dark Beginning 1",
        "set_code": "DB1-EN198",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.13"
      },
      {
        "set_name": "Pharaoh's Servant",
        "set_code": "PSV-088",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.01"
      },
      {
        "set_name": "Pharaoh's Servant",
        "set_code": "PSV-E088",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Pharaoh's Servant",
        "set_code": "PSV-EN088",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Retro Pack 2",
        "set_code": "RP02-EN022",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.2"
      },
      {
        "set_name": "Starter Deck: Yugi Reloaded",
        "set_code": "YSYR-EN010",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.09"
      }
    ],
    "card_images": [
      {
        "id": 83994646,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/83994646.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/83994646.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.09",
        "tcgplayer_price": "0.14",
        "ebay_price": "0.99",
        "amazon_price": "1.29",
        "coolstuffinc_price": "0.25"
      }
    ]
  },
  {
    "id": 23771716,
    "name": "7 Colored Fish",
    "type": "Normal Monster",
    "desc": "A rare rainbow fish that has never been caught by mortal man.",
    "atk": 1800,
    "def": 800,
    "level": 4,
    "race": "Fish",
    "attribute": "WATER",
    "card_sets": [
      {
        "set_name": "Gold Series",
        "set_code": "GLD1-EN001",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-098",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.25"
      },
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-E098",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-EN098",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Starter Deck: Joey",
        "set_code": "SDJ-008",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.16"
      },
      {
        "set_name": "Structure Deck: Fury from the Deep",
        "set_code": "SD4-EN002",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.23"
      }
    ],
    "card_images": [
      {
        "id": 23771716,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/23771716.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/23771716.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.09",
        "tcgplayer_price": "0.25",
        "ebay_price": "2.99",
        "amazon_price": "0.99",
        "coolstuffinc_price": "0.25"
      }
    ]
  },
  {
    "id": 86198326,
    "name": "7 Completed",
    "type": "Spell Card",
    "desc": "Activate this card by choosing ATK or DEF; equip only to a Machine monster. It gains 700 ATK or DEF, depending on the choice.",
    "race": "Equip",
    "card_sets": [
      {
        "set_name": "Battle Pack 3: Monster League",
        "set_code": "BP03-EN135",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.16"
      },
      {
        "set_name": "Battle Pack 3: Monster League",
        "set_code": "BP03-EN135",
        "set_rarity": "Shatterfoil Rare",
        "set_rarity_code": "(SHR)",
        "set_price": "1.16"
      },
      {
        "set_name": "Duel Terminal 2",
        "set_code": "DT02-EN038",
        "set_rarity": "Duel Terminal Normal Parallel Rare",
        "set_rarity_code": "(DNPR)",
        "set_price": "0.00"
      },
      {
        "set_name": "Pharaoh's Servant",
        "set_code": "PSV-004",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.03"
      },
      {
        "set_name": "Pharaoh's Servant",
        "set_code": "PSV-E004",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Pharaoh's Servant",
        "set_code": "PSV-EN004",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Speed Duel: Scars of Battle",
        "set_code": "SBSC-EN029",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.12"
      }
    ],
    "card_images": [
      {
        "id": 86198326,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/86198326.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/86198326.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.08",
        "tcgplayer_price": "0.21",
        "ebay_price": "2.50",
        "amazon_price": "2.21",
        "coolstuffinc_price": "0.25"
      }
    ]
  },
  {
    "id": 41356845,
    "name": "Acid Trap Hole",
    "type": "Trap Card",
    "desc": "Target 1 face-down Defense Position monster on the field; flip it face-up, then destroy it if its DEF is 2000 or less, or return it face-down if its DEF is more than 2000.",
    "race": "Normal",
    "archetype": "Hole",
    "card_sets": [
      {
        "set_name": "Legendary Collection 3: Yugi's World Mega Pack",
        "set_code": "LCYW-EN283",
        "set_rarity": "Rare",
        "set_rarity_code": "(R)",
        "set_price": "1.14"
      },
      {
        "set_name": "Millennium Pack",
        "set_code": "MIL1-EN044",
        "set_rarity": "Rare",
        "set_rarity_code": "(R)",
        "set_price": "1.11"
      },
      {
        "set_name": "Shadow of Infinity: Special Edition",
        "set_code": "SOI-ENSE1",
        "set_rarity": "Secret Rare",
        "set_rarity_code": "(ScR)",
        "set_price": "0.00"
      },
      {
        "set_name": "Shadow of Infinity: Special Edition",
        "set_code": "SOI-ENSE1",
        "set_rarity": "Ultra Rare",
        "set_rarity_code": "(UR)",
        "set_price": "0.00"
      },
      {
        "set_name": "Speed Duel: Battle City Box",
        "set_code": "SBCB-EN102",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.14"
      },
      {
        "set_name": "Structure Deck: Marik (TCG)",
        "set_code": "SDMA-EN029",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.16"
      },
      {
        "set_name": "Yu-Gi-Oh! Dark Duel Stories promotional cards",
        "set_code": "DDS-005",
        "set_rarity": "Prismatic Secret Rare",
        "set_rarity_code": "(PScR)",
        "set_price": "2.28"
      },
      {
        "set_name": "Yu-Gi-Oh! Worldwide Edition: Stairway to the Destined Duel promotional cards",
        "set_code": "SDD-E002",
        "set_rarity": "Prismatic Secret Rare",
        "set_rarity_code": "(PScR)",
        "set_price": "0.00"
      }
    ],
    "card_images": [
      {
        "id": 41356845,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/41356845.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/41356845.jpg"
      },
      {
        "id": 41356846,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/41356846.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/41356846.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.08",
        "tcgplayer_price": "0.20",
        "ebay_price": "2.48",
        "amazon_price": "0.25",
        "coolstuffinc_price": "0.25"
      }
    ]
  },
  {
    "id": 95174353,
    "name": "Ameba",
    "type": "Effect Monster",
    "desc": "When the control of this face-up card on the field shifts to your opponent, inflict 2000 points of damage to your opponent's Life Points. This effect can only be used once as long as this card remains face-up on the field.",
    "atk": 300,
    "def": 350,
    "level": 1,
    "race": "Aqua",
    "attribute": "WATER",
    "card_sets": [
      {
        "set_name": "Dark Beginning 1",
        "set_code": "DB1-EN008",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.23"
      },
      {
        "set_name": "Magic Ruler",
        "set_code": "MRL-010",
        "set_rarity": "Rare",
        "set_rarity_code": "(R)",
        "set_price": "1.19"
      },
      {
        "set_name": "Magic Ruler",
        "set_code": "MRL-E010",
        "set_rarity": "Rare",
        "set_rarity_code": "(R)",
        "set_price": "0.00"
      },
      {
        "set_name": "Spell Ruler",
        "set_code": "SRL-010",
        "set_rarity": "Rare",
        "set_rarity_code": "(R)",
        "set_price": "1.39"
      },
      {
        "set_name": "Spell Ruler",
        "set_code": "SRL-EN010",
        "set_rarity": "Rare",
        "set_rarity_code": "(R)",
        "set_price": "0.00"
      }
    ],
    "card_images": [
      {
        "id": 95174353,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/95174353.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/95174353.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.20",
        "tcgplayer_price": "0.32",
        "ebay_price": "2.92",
        "amazon_price": "0.96",
        "coolstuffinc_price": "0.25"
      }
    ]
  },
  {
    "id": 42431843,
    "name": "Ancient Brain",
    "type": "Normal Monster",
    "desc": "A fallen fairy that is powerful in the dark.",
    "atk": 1000,
    "def": 700,
    "level": 3,
    "race": "Fiend",
    "attribute": "DARK",
    "card_sets": [
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-082",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.16"
      },
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-E082",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-EN082",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Speed Duel: Trials of the Kingdom",
        "set_code": "SBTK-EN004",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.1"
      }
    ],
    "card_images": [
      {
        "id": 42431843,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/42431843.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/42431843.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.03",
        "tcgplayer_price": "0.17",
        "ebay_price": "1.20",
        "amazon_price": "1.20",
        "coolstuffinc_price": "0.25"
      }
    ]
  },
  {
    "id": 93221206,
    "name": "Ancient Elf",
    "type": "Normal Monster",
    "desc": "This elf is rumored to have lived for thousands of years. He leads an army of spirits against his enemies.",
    "atk": 1450,
    "def": 1200,
    "level": 4,
    "race": "Spellcaster",
    "attribute": "LIGHT",
    "card_sets": [
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-037",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.12"
      },
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-E037",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-EN037",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Starter Deck: Yugi",
        "set_code": "SDY-024",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.15"
      },
      {
        "set_name": "Starter Deck: Yugi",
        "set_code": "SDY-E022",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      }
    ],
    "card_images": [
      {
        "id": 93221206,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/93221206.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/93221206.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.09",
        "tcgplayer_price": "0.30",
        "ebay_price": "1.90",
        "amazon_price": "1.20",
        "coolstuffinc_price": "0.25"
      }
    ]
  },
  {
    "id": 43230671,
    "name": "Ancient Lizard Warrior",
    "type": "Normal Monster",
    "desc": "Before the dawn of man, this lizard warrior ruled supreme.",
    "atk": 1400,
    "def": 1100,
    "level": 4,
    "race": "Reptile",
    "attribute": "EARTH",
    "card_sets": [
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-050",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.05"
      },
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-E050",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Metal Raiders",
        "set_code": "MRD-EN050",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      }
    ],
    "card_images": [
      {
        "id": 43230671,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/43230671.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/43230671.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.42",
        "tcgplayer_price": "0.19",
        "ebay_price": "2.50",
        "amazon_price": "0.20",
        "coolstuffinc_price": "0.39"
      }
    ]
  },
  {
    "id": 14015067,
    "name": "Ancient One of the Deep Forest",
    "type": "Normal Monster",
    "desc": "This creature adopts the form of a white goat living in the forest, but is actually a Forest Elder.",
    "atk": 1800,
    "def": 1900,
    "level": 6,
    "race": "Beast",
    "attribute": "EARTH",
    "card_sets": [
      {
        "set_name": "Magic Ruler",
        "set_code": "MRL-018",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.96"
      },
      {
        "set_name": "Magic Ruler",
        "set_code": "MRL-E018",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      },
      {
        "set_name": "Spell Ruler",
        "set_code": "SRL-018",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.11"
      },
      {
        "set_name": "Spell Ruler",
        "set_code": "SRL-EN018",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      }
    ],
    "card_images": [
      {
        "id": 14015067,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/14015067.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/14015067.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.22",
        "tcgplayer_price": "0.17",
        "ebay_price": "1.99",
        "amazon_price": "0.20",
        "coolstuffinc_price": "0.25"
      }
    ]
  },
  {
    "id": 17092736,
    "name": "Ancient Telescope",
    "type": "Spell Card",
    "desc": "See the top 5 cards of your opponent's Deck. Return the cards to the Deck in the same order.",
    "race": "Normal",
    "card_sets": [
      {
        "set_name": "Starter Deck: Kaiba",
        "set_code": "SDK-039",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "1.12"
      },
      {
        "set_name": "Starter Deck: Kaiba",
        "set_code": "SDK-E036",
        "set_rarity": "Common",
        "set_rarity_code": "(C)",
        "set_price": "0.00"
      }
    ],
    "card_images": [
      {
        "id": 17092736,
        "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/17092736.jpg",
        "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/17092736.jpg"
      }
    ],
    "card_prices": [
      {
        "cardmarket_price": "0.10",
        "tcgplayer_price": "0.46",
        "ebay_price": "0.99",
        "amazon_price": "1.30",
        "coolstuffinc_price": "0.25"
      }
    ]
  },
];
