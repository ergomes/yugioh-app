import React from 'react';
import './App.css';
// 1. importar cards.js


// Exemplo de como mostrar cada carta, 300px wide
/**
 --------------------
 |                  |
 |                  |
 |                  |
 |                  |
 --------------------
 Nome da carta
 Ebay price: $XXX

 Lorem ipsum dolor sit
 amet, consectetur sed
 adipiscing elit,  ...
 */



// 2. Criar componente pra renderizar uma carta
//    Componente recebe os dados da carta na props.card
//    Component usa apenas divs (no p, span, table, etc etc... somente divs)
// 2.1. Mostrar imagem, nome, descricao e preco no ebay
// 2.2. Usar CSS pra mostrar tudo bonitinho
function Card(props) {
}


// 6. Criar o mesmo component card, agora usando uma classe ao inves de uma funcao
class Card2 extends React.Component {
}


// 3. Adicionar um titulo (i.e. h1)
// 4. User "map" pra renderizar todas as cartas (i.e. cards.map(c => <Card card={c} />)
// 7. Adicionar um novo titulo para a segunda seccao (i.e. h1)
// 8. Usar "map" pra renderizar todas as cartas (i.e. cards.map(c => <Card2 card={c} />)
function App() {
  return (
    <div className="App">
    </div>
  );
}

export default App;






